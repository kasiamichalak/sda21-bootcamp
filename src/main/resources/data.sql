insert into COURSE (DATE_START, DATE_END, MODE, NAME, PRICE)
values (DATE '2020-03-01', DATE '2020-03-31', 'dzienny', 'Java', 8000);
insert into COURSE (DATE_START, DATE_END, MODE, NAME, PRICE)
values (DATE '2020-04-01', DATE '2020-04-30', 'wieczorowy', 'Phyton', 8000);
insert into COURSE (DATE_START, DATE_END, MODE, NAME, PRICE)
values (DATE '2020-05-01', DATE '2020-05-31', 'weekendowy', 'SQL', 2000);

insert into ROLE (NAME) values ('admin');
insert into ROLE (NAME) values ('student');
insert into ROLE (NAME) values ('trainer');

insert into USER (EMAIL, FIRST_NAME, LAST_NAME, PASSWORD, PHONE_NUMBER, ROLE_ID)
values ('jk@o2.pl', 'Joanna', 'Kowalska', '$2y$11$t24f6vxABfwFfifvL1KkFO/WV4nPTlVodF2DyA6QbGudi.Z9TLL3m', '345-123-567', 1);
insert into USER (EMAIL, FIRST_NAME, LAST_NAME, PASSWORD, PHONE_NUMBER, ROLE_ID)
values ('oo@o2.pl', 'Olga', 'Okulska', '$2y$11$PWC68fYmhptsD1LP3KjIKu8kdTFfI7r5dvEbXos9ofEcc2KsRCEE2', '654-345-567', 2);
insert into USER (EMAIL, FIRST_NAME, LAST_NAME, PASSWORD, PHONE_NUMBER, ROLE_ID)
values ('ab@o2.pl', 'August', 'Branicki', '$2y$11$v7LRM7Z29PMR7dfYIUirY.jEFSSCba6351NbUTag.CpYSch/pdKbW', '456-345-567', 3);
insert into USER (EMAIL, FIRST_NAME, LAST_NAME, PASSWORD, PHONE_NUMBER, ROLE_ID)
values ('bg@o2.pl', 'Barbara', 'Godocka', '$2y$11$UbGZOUZLQ82JeGLU.O4Up.8XSc6cQ3sAQuq8j9Q/OpkbQ2IRN2sNO', '601-324-767', 3);

insert into USER_COURSES (USER_ID, COURSES_ID) values (2, 1);
insert into USER_COURSES (USER_ID, COURSES_ID) values (2, 2);
insert into USER_COURSES (USER_ID, COURSES_ID) values (3, 1);
insert into USER_COURSES (USER_ID, COURSES_ID) values (4, 2);
