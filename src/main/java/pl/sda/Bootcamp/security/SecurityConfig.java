package pl.sda.Bootcamp.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
//    public class AppConfig extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {

    @Autowired
    private DataSource dataSource;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        PasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

        auth.jdbcAuthentication()
                // sprawdzenie i pobranie loginu i hasła użytkownika po adresie email
                // który jest u nas w systemie loginem
                // tu zapytanie jest czystoSQLowe
                .usersByUsernameQuery("select email, password, 1 from user where email=?")
                //pobranie roli uzytkownika z bazy danych po podanym emailu(loginie)
                .authoritiesByUsernameQuery("select u.email, r.name from user u inner join role r " +
                        "on r.id=u.role_id where u.email=?")
                //ustawienie klasy odpowiedzialnej za nawiazanie połączenia do bazy danych
                .dataSource(dataSource)
                //ustawienie sposobu kodowania hasła w bazie danych
                .passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()
                .antMatchers("/admin/**", "/admin").hasAuthority("admin")
                .antMatchers("/user/**", "/user").hasAnyAuthority("admin", "student", "trainer")
                .antMatchers("/student/**", "/student").hasAuthority("student")
                .antMatchers("/trener/**", "/trener").hasAuthority("trainer")
                .anyRequest().permitAll()
                .and()
                .formLogin()
                .loginPage("/templates/login")
                .loginProcessingUrl("/login")
                .defaultSuccessUrl("/dashboard")
                .failureUrl("/login-error")
                .usernameParameter("email")
                .passwordParameter("password")
                .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/");
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/h2-console/**");
    }

//    @Override
//    public void addViewControllers(ViewControllerRegistry registry) {
//        registry.addViewController("/login").setViewName("login");
//    }
}
