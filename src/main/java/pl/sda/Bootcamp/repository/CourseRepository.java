package pl.sda.Bootcamp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.sda.Bootcamp.model.Course;

import java.util.List;

//pierwszy parametr to typ obiektu, którego dotyczy Repository
// drugi parametr to typ klucza głównego

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {

    List<Course> findAll();

    //    metoda zwraca kursy po nazwie
    List<Course> findByName(String name);

    List<Course> findByNameAndPriceGreaterThanEqual(String name, int minPrice);

    @Query("select c from Course c where c.name = ?1 and c.price > ?2")
    List<Course> findByNameAndPriceQuery(String name, int price);

    @Query("select c from Course c where c.name = :name and c.price > :price")
    List<Course> findByNameAndPriceQueryParam(@Param("name") String name,
                                              @Param("price") int price);

    @Query(name = "Course.findByMode")
    List<Course> findByModeQuery(@Param("mode") String mode);

}
