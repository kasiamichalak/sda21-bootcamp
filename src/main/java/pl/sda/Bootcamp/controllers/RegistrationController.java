package pl.sda.Bootcamp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.Bootcamp.model.Course;
import pl.sda.Bootcamp.model.User;
import pl.sda.Bootcamp.service.CourseService;
import pl.sda.Bootcamp.service.RoleService;
import pl.sda.Bootcamp.service.UserService;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

@Controller
@RequestMapping(value = "/rejestracja")
public class RegistrationController {

    @Autowired
    private UserService userService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private RoleService roleService;

    @GetMapping(value = "")
    public String registration(Model model) {
        model.addAttribute("user", new User());
        List<Course> courseList = courseService.findAll();
        model.addAttribute("courselist", courseList);
        return "registration/registration";
    }

    // bindingResult musi być bezpośrednio po modelu walidowanym w parametrach
    @PostMapping(value = "/podsumowanie")
    public String summaryPost(@Valid @ModelAttribute User user, BindingResult bindingResult, Model model) {

        // walidacja błędów formularza
        if (bindingResult.hasErrors()) {

            List<String> errors = new LinkedList<>();
            bindingResult.getAllErrors().forEach(error -> errors.add(error.getDefaultMessage()));
            model.addAttribute("errorlist", errors);

            List<Course> courseList = courseService.findAll();
            model.addAttribute("courselist", courseList);

            return "registration/registration";
        }
            user.setRole(roleService.findByName("student"));
            // kodowanie hasła studenta w bazie danych
            PasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            model.addAttribute("user", user);
            userService.save(user);

        return "registration/summary";
    }
}
