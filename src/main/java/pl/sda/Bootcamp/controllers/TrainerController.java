package pl.sda.Bootcamp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sda.Bootcamp.model.*;
import pl.sda.Bootcamp.service.CourseService;
import pl.sda.Bootcamp.service.RoleService;
import pl.sda.Bootcamp.service.UserService;

import java.util.List;

@Controller
@RequestMapping(value = "/admin/trener")
public class TrainerController {

    @Autowired
    UserService userService;

    @Autowired
    RoleService roleService;

    @Autowired
    CourseService courseService;

    @GetMapping(value = "/lista")
    public String displayTrainerList(Model model) {
        List<User> trainers = userService.findByRole(roleService.findByName("trainer"));
        model.addAttribute("trainerlist", trainers);
        return "trainer/trainerlist";
    }

    @GetMapping(value = "/dodaj")
    public String displayFormAddTrainer(Model model) {
        List<Course> courseList = courseService.findAll();
        model.addAttribute("courselist", courseList);
        model.addAttribute("user", new User());
        return "trainer/add";
    }

    @PostMapping(value = "/lista")
    public String saveAddTrainer(@ModelAttribute User user,
                            Model model) {
        user.setRole(roleService.findByName("trainer"));
        PasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userService.save(user);
        return "redirect:lista";
    }
    @GetMapping(value = "/usun")
    public String deleteTrainer(@RequestParam Long id) {
        userService.delete(id);
        return "redirect:lista";
    }
// ładniejsze linki i można ustawiać dynamiczne parametry również w środku linka
//    @GetMapping(value = "/edytuj/{userId}")
//    public String editTrainer(@PathVariable("userId") Long id, Model model) {
    @GetMapping(value = "/edytuj")
    public String displayFormEditTrainer(@RequestParam Long id, Model model) {
        model.addAttribute("user", userService.getUserById(id));
        List<Course> courseList = courseService.findAll();
        model.addAttribute("courselist", courseList);
        return "trainer/edit";
    }

    @PostMapping(value = "/zapisz")
    public String saveEditTrainer(@ModelAttribute User user, Model model) {
        User currentUser = userService.getUserById(user.getId());
        user.setRole(currentUser.getRole());
        user.setPassword(currentUser.getPassword());
        userService.save(user);
        return "redirect:lista";
    }
}
