package pl.sda.Bootcamp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sda.Bootcamp.model.User;
import pl.sda.Bootcamp.service.CourseService;
import pl.sda.Bootcamp.service.RoleService;
import pl.sda.Bootcamp.service.UserService;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping(value = "/")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    RoleService roleService;

    @Autowired
    CourseService courseService;

    @GetMapping(value = "/login")
    String displayLogin(Model model) {
        return "login";
    }

    @GetMapping("/login-error")
    public String loginError(Model model) {
        model.addAttribute("loginError", true);
        return "login";
    }

    @GetMapping(value = "/dashboard")
    public String displayDashboard(Principal principal, Model model) {
        List<User> users = userService.findAll();
        User currentUser = new User();
        for (User user : users) {
            if (principal.getName().equals(user.getEmail())) {
                currentUser = user;
            }
        }
        model.addAttribute("user", currentUser);
        return "user/dashboard";
    }
}
