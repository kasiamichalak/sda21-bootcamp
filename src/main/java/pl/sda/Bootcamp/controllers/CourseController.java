package pl.sda.Bootcamp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sda.Bootcamp.model.Course;
import pl.sda.Bootcamp.model.User;
import pl.sda.Bootcamp.service.CourseService;

import java.util.List;

@Controller
@RequestMapping(value = CourseController.BASE_URL)
public class CourseController {

    public static final String BASE_URL = "/admin/course";
    private final CourseService courseService;

    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }

    @GetMapping(value = "/list")
    public String courseList(Model model) {
        List<Course> courseList = courseService.findAll();
        model.addAttribute("courselist", courseList);
        return "course/courselist";
    }

    @GetMapping(value = "/add")
    public String displayFormAddCourse(Model model) {
        model.addAttribute("course", new Course());
        return "course/add";
    }

    @PostMapping(value = "/add")
    public String addCourse(@ModelAttribute Course course,
                            Model model) {
        courseService.save(course);
        return "redirect:list";
    }

    @GetMapping(value = "/delete")
    public String deleteCourse(@RequestParam Long id) {
        courseService.delete(id);
        return "redirect:list";
    }

    @GetMapping(value = "/edit")
    public String editCourse(@RequestParam Long id, Model model) {
        model.addAttribute("course", courseService.getCourseById(id));
        return "course/add";
    }
}
