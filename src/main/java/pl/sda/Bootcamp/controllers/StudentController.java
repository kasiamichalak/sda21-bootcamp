package pl.sda.Bootcamp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sda.Bootcamp.model.Course;
import pl.sda.Bootcamp.model.User;
import pl.sda.Bootcamp.service.CourseService;
import pl.sda.Bootcamp.service.RoleService;
import pl.sda.Bootcamp.service.UserService;

import java.util.List;

@Controller
@RequestMapping(value = "/admin/student")
public class StudentController {

    @Autowired
    UserService userService;

    @Autowired
    RoleService roleService;

    @Autowired
    CourseService courseService;

    @GetMapping(value = "/lista")
    public String displayStudentList(Model model) {
        List<User> students = userService.findByRole(roleService.findByName("student"));
        model.addAttribute("studentlist", students);
        return "student/studentlist";
    }

    @GetMapping(value = "/dodaj")
    public String displayFormAddStudent(Model model) {
        List<Course> courseList = courseService.findAll();
        model.addAttribute("courselist", courseList);
        model.addAttribute("user", new User());
        return "student/add";
    }

    @PostMapping(value = "/lista")
    public String saveAddStudent(@ModelAttribute User user,
                            Model model) {
        user.setRole(roleService.findByName("student"));
        PasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userService.save(user);
        return "redirect:lista";
    }
    @GetMapping(value = "/usun")
    public String deleteStudent(@RequestParam Long id) {
        userService.delete(id);
        return "redirect:lista";
    }
    @GetMapping(value = "/edytuj")
    public String displayFormEditStudent(@RequestParam Long id, Model model) {
        model.addAttribute("user", userService.getUserById(id));
        List<Course> courseList = courseService.findAll();
        model.addAttribute("courselist", courseList);
        return "student/edit";
    }

    @PostMapping(value = "/zapisz")
    public String saveEditStudent(@ModelAttribute User user, Model model) {
        User currentUser = userService.getUserById(user.getId());
        user.setRole(currentUser.getRole());
        user.setPassword(currentUser.getPassword());
        userService.save(user);
        return "redirect:lista";
    }
}
