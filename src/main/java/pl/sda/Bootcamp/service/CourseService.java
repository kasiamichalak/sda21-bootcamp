package pl.sda.Bootcamp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.Bootcamp.model.Course;
import pl.sda.Bootcamp.model.User;
import pl.sda.Bootcamp.repository.CourseRepository;

import java.util.List;

@Service
public class CourseService {

    private final CourseRepository courseRepository;

    public CourseService(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    public List<Course> findAll() {
        return courseRepository.findAll();
    }

    public Course getCourseById(Long id) {
        return courseRepository.getOne(id);
    }

    public void save(Course course) {
        courseRepository.save(course);
    }

    public void delete(Long id) {
        courseRepository.deleteById(id);
    }

    public List<Course> findByName(String name) {
        return courseRepository.findByName(name);
    }

    public List<Course> findByNameAndPriceGreaterThanEqual(String name, int minPrice) {
        return courseRepository.findByNameAndPriceGreaterThanEqual(name, minPrice);
    }

    public List<Course> findByNameAndPriceQuery(String name, int price) {
        return courseRepository.findByNameAndPriceQuery(name, price);
    }

    public List<Course> findByModeQuery(String mode) {
        return courseRepository.findByModeQuery(mode);
    }
}
