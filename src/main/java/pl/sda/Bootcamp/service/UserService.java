package pl.sda.Bootcamp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.Bootcamp.model.Role;
import pl.sda.Bootcamp.model.User;
import pl.sda.Bootcamp.repository.UserRepository;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<User> findAll() { return userRepository.findAll(); }

    public List<User> findAllByRole_Id(Long id) {
        return userRepository.findAllByRole_Id(id);
    }

    public User getUserById(Long id) {
        return userRepository.getOne(id);
    }

//    public List<User> findByEmail(String email) {
//        return userRepository.findByEmail(email);
//    }

    public void delete(Long id) {
        userRepository.deleteById(id);
    }

    public void save(User user) {
        userRepository.save(user);
    }

    public List<User> findByRole(Role role) {
        return userRepository.findByRole(role);
    }

}
