package pl.sda.Bootcamp.model;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "first_name")
    @NotEmpty(message = "{pl.sda.Bootcamp.model.User.firstName.NotEmpty}")
    private String firstName;
    @Column(name = "last_name")
    @NotBlank(message = "{NotBlank}")
    private String lastName;
    @Email
    @NotBlank(message = "{NotBlank}")
    private String email;
    @NotBlank(message = "{NotBlank}")
    private String password;
    @Column(name = "phone_number")
    private String phoneNumber;

    @ManyToMany
    @NotEmpty(message = "{pl.sda.Bootcamp.model.User.courses.NotEmpty}")
    private List<Course> courses;

    @ManyToOne
    @JoinColumn(name="role_id")
    private Role role;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

}
