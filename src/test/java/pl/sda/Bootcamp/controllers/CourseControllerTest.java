package pl.sda.Bootcamp.controllers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.sda.Bootcamp.model.Course;
import pl.sda.Bootcamp.repository.CourseRepository;
import pl.sda.Bootcamp.service.CourseService;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static pl.sda.Bootcamp.controllers.CourseController.BASE_URL;

class CourseControllerTest {

    @Mock
    CourseService courseService;
    CourseController courseController;
    MockMvc mockMvc;
    private static final Long ID = 1L;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        courseController = new CourseController(courseService);
        mockMvc = MockMvcBuilders.standaloneSetup(courseController).build();
    }

    @Test
    void testCourseList() throws Exception {
        List<Course> courses = new ArrayList<>(List.of(new Course(), new Course()));
        when(courseService.findAll()).thenReturn(courses);

        mockMvc.perform(get(BASE_URL + "/list"))
                .andExpect(status().isOk())
                .andExpect(view().name("course/courselist"))
                .andExpect(model().attributeExists("courselist"));

        verify(courseService, times(1)).findAll();
    }

    @Test
    void testDisplayFormAddCourse() throws Exception {
        mockMvc.perform(get(BASE_URL + "/add"))
                .andExpect(status().isOk())
                .andExpect(view().name("course/add"))
                .andExpect(model().attributeExists("course"));
    }

    @Test
    void testAddCourse() throws Exception {
        mockMvc.perform(post(BASE_URL + "/add")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("course.id", String.valueOf(ID))
                .param("course.name", "Spring")
                .param("course.dateStart", "2021-04-03")
                .param("course.dateEnd", "2021-04-06")
                .param("course.price", "5000")
                .param("course.mode", "dzienny")
                .flashAttr("course", new Course()))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:list"));

        verify(courseService, times(1)).save(any(Course.class));
    }

    @Test
    void testDeleteCourse() throws Exception {
        mockMvc.perform(get(BASE_URL + "/delete")
                .param("id", "1"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:list"));

        verify(courseService, times(1)).delete(anyLong());
    }

    @Test
    void testEditCourse() throws Exception {
        Course course = new Course();
        course.setId(ID);

        when(courseService.getCourseById(anyLong())).thenReturn(course);

        mockMvc.perform(get(BASE_URL + "/edit")
                .param("id", String.valueOf(ID)))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("course"))
                .andExpect(view().name("course/add"));
    }
}