package pl.sda.Bootcamp.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import pl.sda.Bootcamp.model.Course;
import pl.sda.Bootcamp.repository.CourseRepository;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;


@ExtendWith(SpringExtension.class)
@SpringBootTest
public class CourseServiceIT {

    public static final Long ID = 1L;

    @Autowired
    CourseService courseService;

    @Autowired
    CourseRepository courseRepository;

    @Transactional
    @Test
    public void testSaveNewCourse() throws Exception {

        Course expectedCourse = new Course();
        expectedCourse.setId(ID);
        expectedCourse.setName("Name");
        expectedCourse.setMode("Mode");
        expectedCourse.setDateStart(LocalDate.of(2021, 3, 31));
        expectedCourse.setDateEnd(LocalDate.of(2021, 4, 30));
        expectedCourse.setPrice(100);

        courseService.save(expectedCourse);

        Course actualCourse = courseRepository.findById(ID).get();

        assertEquals(expectedCourse.getId(), actualCourse.getId());
        assertEquals(expectedCourse.getMode(), actualCourse.getMode());
    }
}
