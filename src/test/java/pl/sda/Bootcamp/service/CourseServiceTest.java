package pl.sda.Bootcamp.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import pl.sda.Bootcamp.model.Course;
import pl.sda.Bootcamp.repository.CourseRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CourseServiceTest {

    CourseService courseService;
    @Mock
    CourseRepository courseRepository;
    static final Long ID = 1L;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        courseService = new CourseService(courseRepository);
    }

    @Test
    void testFindAll() {
        Course course = new Course();
        List<Course> expectedCourses = new ArrayList<>();
        expectedCourses.add(course);

        when(courseRepository.findAll()).thenReturn(expectedCourses);

        List<Course> actualCourses = courseService.findAll();

        assertEquals(expectedCourses.size(), actualCourses.size());
        verify(courseRepository, times(1)).findAll();
        verify(courseRepository, never()).findById(anyLong());
    }

    @Test
    void testGetCourseById() {
        Course expectedCourse = new Course();
        expectedCourse.setId(ID);

        when(courseRepository.getOne(anyLong())).thenReturn(expectedCourse);
        Course actualCourse = courseService.getCourseById(ID);

        assertEquals(expectedCourse, actualCourse);
        assertNotNull(actualCourse, "Null course returned");
        verify(courseRepository, times(1)).getOne(anyLong());
        verify(courseRepository, never()).findAll();
    }

    @Test
    void testDeleteCourse() {
        courseService.delete(ID);

        verify(courseRepository, times(1)).deleteById(anyLong());
    }
}